package app.flux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AppConfig {

	@Autowired
	private WebClient.Builder webClientBuilder;	


	@Bean("covidWebClient")
	public WebClient covidWebClient() {
		return webClientBuilder.baseUrl("https://api.covid19api.com").build();
	}
	
}
