package app.flux.covid;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import app.flux.covid.json.CovidSummary;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/covid")
public class CovidApi {

	private ModelMapper modelMapper;
	
	
	@Autowired
	private CovidRepository covidRepository;
	
	@Autowired
	@Qualifier("covidWebClient")
	private WebClient webClient;
	
	public CovidApi() {
		modelMapper = new ModelMapper();
	}
	
	@GetMapping("/summary")
	public Mono<CovidSummary> getSummary() {
		log.info("getSummary Begin");
		return webClient.get()
			.uri("/summary")
			.retrieve()
			.bodyToMono(CovidSummary.class)
			.elapsed()
			.doOnNext(tuple -> log.info("getSummary Elapsed {}", tuple.getT1()))
			.map(tuple -> tuple.getT2());
	}

	@GetMapping("/list")
	public Flux<Covid> getCovidList() {
		return webClient.get()
			.uri("/summary")
			.retrieve()
			.bodyToMono(CovidSummary.class)
			.flatMapIterable(covidSummary -> convertToList(covidSummary))
			.filter(covid -> filterByTotal(covid))
			.delayElements(Duration.ofMillis(1000));
	}
	
	@GetMapping(path = "/stream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Flux<Covid> getCovidStream() {
		return webClient.get()
			.uri("/summary")
			.retrieve()
			.bodyToMono(CovidSummary.class)
			.flatMapIterable(covidSummary -> convertToList(covidSummary))
			.filter(covid -> filterByTotal(covid))
			.delayElements(Duration.ofMillis(1000));
	}
		
	@GetMapping("/save")
	public Flux<Covid> getCovidSave() {
		log.info("getCovidSave Begin");
		return webClient.get()			
			.uri("/summary")
			.retrieve()
			.bodyToMono(CovidSummary.class)
			.flatMapIterable(covidSummary -> convertToList(covidSummary))
			.filter(covid -> filterByTotal(covid))
			.doOnNext(covid -> saveCovid(covid))
			.elapsed()
			.doOnNext(tuple -> log.info("getCovidSave Elapsed {}", tuple.getT1()))
			.map(tuple -> tuple.getT2());
	}
		
	private List<Covid> convertToList(CovidSummary summary) {
		 List<Covid> list = summary.getCountries().stream().map(c -> modelMapper.map(c, Covid.class)).collect(Collectors.toList());
		 Covid global = modelMapper.map(summary.getGlobal(), Covid.class);
		 global.setSlug("global");
		 global.setDate(summary.getDate());
		 list.add(global);
		 return list;
	}
	
	
	private boolean filterByTotal(Covid covid) {
		return covid.getTotalConfirmed() > 200000;
	}

		
	private Covid saveCovid(Covid covid) {
		return covidRepository.save(covid);
	}
	

	@GetMapping("/list/save")
	public Mono<List<Covid>> getCovidListSave() {
		log.info("getCovidListSave Begin");
		return webClient.get()			
			.uri("/summary")
			.retrieve()
			.bodyToMono(CovidSummary.class)
			.flatMapIterable(covidSummary -> convertToList(covidSummary))
			.filter(covid -> filterByTotal(covid))
			.doOnNext(covid -> saveCovid(covid))
			.collectList()
			.elapsed()
			.doOnNext(tuple -> log.info("getCovidListSave Elapsed {}", tuple.getT1()))
			.map(tuple -> tuple.getT2());
	}
	
}
