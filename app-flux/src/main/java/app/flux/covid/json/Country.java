package app.flux.covid.json;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"Country","CountryCode","Slug","NewConfirmed","TotalConfirmed","NewDeaths","TotalDeaths","NewRecovered","TotalRecovered","Date"})
public class Country {

	@JsonProperty("Country")
	private String country;
	@JsonProperty("CountryCode")
	private String countryCode;
	@JsonProperty("Slug")
	private String slug;
	@JsonProperty("NewConfirmed")
	private Integer newConfirmed;
	@JsonProperty("TotalConfirmed")
	private Integer totalConfirmed;
	@JsonProperty("NewDeaths")
	private Integer newDeaths;
	@JsonProperty("TotalDeaths")
	private Integer totalDeaths;
	@JsonProperty("NewRecovered")
	private Integer newRecovered;
	@JsonProperty("TotalRecovered")
	private Integer totalRecovered;
	@JsonProperty("Date")
	private Date date;
}
