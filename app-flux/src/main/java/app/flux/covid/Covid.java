package app.flux.covid;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "covid")
public class Covid {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String slug;	
	private Integer totalConfirmed;	
	private Integer totalDeaths;	
	private Integer totalRecovered;	
	private Date date;
	
}
