package app.boot.empleado;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmpleadoDto {

	private Long id;
	
	@NotNull
	@NotBlank
	private String nombre;  
	private String correo;
	
	@DecimalMax(value = "500.0")
	private Double salarioHora;
	
}
