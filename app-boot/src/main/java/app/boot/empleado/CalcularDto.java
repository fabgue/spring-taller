package app.boot.empleado;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CalcularDto {

	private Integer horas;
	private Long id;
	
}
