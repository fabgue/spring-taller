package app.boot.empleado;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;


@Component
public class EmpleadoAssembler {

	public EmpleadoDto convertToDto(Empleado empleado) {
		EmpleadoDto empleadoDto = new EmpleadoDto();
		empleadoDto.setId(empleado.getId());
		empleadoDto.setNombre(empleado.getNombre());
		empleadoDto.setCorreo(empleado.getCorreo());
		empleadoDto.setSalarioHora(empleado.getSalarioHora());
		return empleadoDto;
	}

	
	public Empleado convertToEntity(EmpleadoDto empleadoDto) {
		Empleado empleado = new Empleado();
		empleado.setId(empleadoDto.getId());
		empleado.setNombre(empleadoDto.getNombre());
		empleado.setCorreo(empleadoDto.getCorreo());
		empleado.setSalarioHora(empleadoDto.getSalarioHora());
		return empleado;
	}

	
	public List<EmpleadoDto> convertToListDto(List<Empleado> empleados) {
		return empleados.stream().map(c -> convertToDto(c)).collect(Collectors.toList());
	}
	
}
