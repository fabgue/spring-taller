package app.boot.empleado;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/empleado")
public class EmpleadoController {

	@Autowired
	private EmpleadoService empleadoService;
	

	@GetMapping
	public List<EmpleadoDto> getAll() {
		return empleadoService.findAll();
	}

	@GetMapping("/{id}")
	public EmpleadoDto getById(@PathVariable Long id) {
		return empleadoService.findById(id); 
	}
	
	@PostMapping
	public EmpleadoDto postCreate(@Valid @RequestBody EmpleadoDto empleadoDto) {
		return empleadoService.create(empleadoDto);
	}

	@PutMapping("/{id}")
	public EmpleadoDto putUpdate(@PathVariable(value = "id") Long id, @Valid @RequestBody EmpleadoDto empleadoDto) {
		return empleadoService.update(empleadoDto);  
	}	
	
	@DeleteMapping("/{id}")
	public Map<String,Object> delete(@PathVariable(value = "id") Long id) {
		empleadoService.deleteById(id);
        return Collections.singletonMap("deleted", Boolean.TRUE);
    }

	
	@PostMapping("/calcular")
	public Map<String,Object> postCalcular(@RequestBody CalcularDto calcularDto) {
		log.info("postCalcular {}", calcularDto);
		Double total = empleadoService.calcularSalario(calcularDto);
		return Collections.singletonMap("total", total);
	}

	@GetMapping("/response/{id}")
	public ResponseEntity<EmpleadoDto> getResponseById(@PathVariable Long id) {
		return new ResponseEntity<EmpleadoDto>(empleadoService.findById(id), HttpStatus.OK); 
	}
	
}
