package app.boot.empleado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.boot.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@Transactional
public class EmpleadoService {

	@Autowired	
	private EmpleadoRepository empleadoRepository;
	
	@Autowired
	private EmpleadoAssembler empleadoAssembler;
	
	private Integer numeroHoras;
	

	public EmpleadoDto findById(Long id) {
		Empleado empleado = empleadoRepository.findById(id).orElseThrow(() -> new ApplicationException("findById"));
		return empleadoAssembler.convertToDto(empleado);
	}

	public List<EmpleadoDto> findAll() {
		List<Empleado> empleados = empleadoRepository.findAll();
		return empleadoAssembler.convertToListDto(empleados);
	}

	public void deleteById(Long id) {
		empleadoRepository.deleteById(id);
	}

	public EmpleadoDto create(EmpleadoDto empleadoDto) {
		Empleado empleado = empleadoAssembler.convertToEntity(empleadoDto);
		empleado = empleadoRepository.save(empleado);
		return empleadoAssembler.convertToDto(empleado);
	}

	public EmpleadoDto update(EmpleadoDto empleadoDto) {
		Empleado empleado = empleadoAssembler.convertToEntity(empleadoDto);
		empleado = empleadoRepository.save(empleado);
		return empleadoAssembler.convertToDto(empleado);
	}
	
	// Mostrar error por uso de this.numeroHoras en componentes
	public Double calcularSalario(CalcularDto dto) {
		log.info("calcularSalario {}", dto);
		double total = 0d;
		this.numeroHoras = dto.getHoras();
		// Procesamiento
		try {
			Thread.sleep(5000);
		} 
		catch (InterruptedException ex) {
			throw new RuntimeException("calcularSalario",ex);
		}
		Empleado empleado = empleadoRepository.findById(dto.getId()).orElse(null);
		if (empleado != null) {
			total = this.numeroHoras * empleado.getSalarioHora();
		}
		log.info("calcularSalario dto={}, numeroHoras={}, total={}", dto, numeroHoras, total);
		return total;
	}

	
}
