package app.boot.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
//@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler  {

	
    @ExceptionHandler(value = ApplicationException.class) 
    protected ResponseEntity<Object> handleApplicationException(ApplicationException ex, WebRequest request) {
    	MensajeError mensajeError = new MensajeError(HttpStatus.INTERNAL_SERVER_ERROR, ex);   	
        String errorUri = ((ServletWebRequest)request).getRequest().getRequestURL().toString();
        mensajeError.setPath(errorUri);
        log.error("Exception: {}", mensajeError, ex);
        return new ResponseEntity<>(mensajeError, mensajeError.getStatus());
    }
	
    
    /*@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	MensajeError mensajeError = new MensajeError(status, ex);
        String errorUri = ((ServletWebRequest)request).getRequest().getRequestURL().toString();
        mensajeError.setPath(errorUri);
        log.error("Exception: {}", mensajeError, ex);
        return new ResponseEntity<>(mensajeError, headers, status);
	}*/
    

}
