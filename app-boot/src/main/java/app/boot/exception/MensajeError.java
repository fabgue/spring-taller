package app.boot.exception;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MensajeError {

	private LocalDateTime timestamp;
	private HttpStatus status;
	private String error;
	private String message;
	private String path;
	private String errorId;


	public MensajeError(HttpStatus status, Throwable ex) {
		this.timestamp = LocalDateTime.now();
		this.errorId = String.valueOf(Timestamp.valueOf(timestamp).getTime());
		this.status = status;
		this.error = ex.getClass().getName();
		this.message = ex.getLocalizedMessage();
	}
}
