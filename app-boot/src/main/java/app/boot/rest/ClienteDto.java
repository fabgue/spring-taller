package app.boot.rest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClienteDto {
	
	private Long id;
	private String tipoDocumento;
	private String numeroDocumento;
	private String nombre;

}
