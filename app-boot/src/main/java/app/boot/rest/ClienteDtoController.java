package app.boot.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
@RequestMapping("/cliente/rest")
public class ClienteDtoController {

	@Autowired
	private RestTemplate restTemplate;
	
	private String endpoint = "http://localhost:8010/cliente";
	
	@GetMapping
	public List<ClienteDto> findAll() {
		HttpHeaders headers = new HttpHeaders();
	    ResponseEntity<List<ClienteDto>> response = new RestTemplate()
	    	.exchange(endpoint, HttpMethod.GET, new HttpEntity<>( headers), new ParameterizedTypeReference<List<ClienteDto>>() {});
	    return response.getBody();
	}
	
	
	@GetMapping("/{id}")
	public ClienteDto findById(@PathVariable Long id) {
		ResponseEntity<ClienteDto> response = restTemplate.getForEntity(endpoint + "/" + id, ClienteDto.class);
		return response.getBody();
	}
	
}
