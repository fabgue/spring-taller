package app.spring.cliente;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	

	@GetMapping
	public List<Cliente> getAll() {
		return clienteService.findAll();
	}

	@GetMapping("/{id}")
	public Cliente getById(@PathVariable Long id) {
		return clienteService.findById(id); 
	}
	
	@PostMapping
	public Cliente postCreate(@RequestBody Cliente cliente) {
		return clienteService.create(cliente);
	}

	@PutMapping("/{id}")
	public Cliente putUpdate(@PathVariable(value = "id") Long id, @RequestBody Cliente cliente) {
		return clienteService.update(cliente);  
	}	
	
	@DeleteMapping("/{id}")
	public Map<String,Object> delete(@PathVariable(value = "id") Long id) {
		clienteService.deleteById(id);
        return Collections.singletonMap("deleted", Boolean.TRUE);	
    }
	
}
