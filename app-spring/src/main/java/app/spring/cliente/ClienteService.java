package app.spring.cliente;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ClienteService {

	@Autowired	
	private ClienteRepository clienteRepository;
	
	
	public Cliente findById(Long id) {
		return clienteRepository.findById(id).orElseThrow(() -> new RuntimeException("Cliente.findById"));
	}

	public List<Cliente> findAll() {
		return clienteRepository.findAll();
	}

	public void deleteById(Long id) {
		clienteRepository.deleteById(id);
	}

	public Cliente create(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	public Cliente update(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
}
