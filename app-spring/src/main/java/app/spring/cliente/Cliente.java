package app.spring.cliente;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "tipo_documento", nullable = false)
	private String tipoDocumento;
	
	@Column(name = "numero_documento", nullable = false)
	private String numeroDocumento;
	
	@Column(name = "nombre", nullable = false)
	private String nombre;

}
